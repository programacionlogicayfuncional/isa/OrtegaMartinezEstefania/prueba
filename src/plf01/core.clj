(ns plf01.core
  (:gen-class))

(defn menor-que-1
  [num1 num2]
  (< num1 num2))

(defn menor-que-2
  [num1 num2 num3]
  (< num1 num2 num3))

(defn menor-que-3
  [num1 num2 num3 num4]
  (< num1 num2 num3 num4))

(menor-que-1 30 20)
(menor-que-2 10 30 30)
(menor-que-3 4 8 38 119)

(defn menor-igual-que-1
  [num1 num2]
  (<= num1 num2))

(defn menor-igual-que-2
  [num1 num2 num3]
  (<= num1 num2 num3))

(defn menor-igual-que-3
  [num1 num2 num3 num4]
  (<= num1 num2 num3 num4))

(menor-igual-que-1 101 100)
(menor-igual-que-2 10 27 27)
(menor-igual-que-3 9 18 11 1)

(defn igual-igual-que-1
  [num1 num2]
  (== num1 num2))

(defn igual-igual-que-2
  [num1 num2 num3]
  (== num1 num2 num3))

(defn igual-igual-que-3
  [num1 num2 num3 num4]
  (== num1 num2 num3 num4))

(igual-igual-que-1 1 1.0)
(igual-igual-que-2 89 90 89)
(igual-igual-que-3 41 41 41 41)

(defn mayor-que-1
  [num1 num2]
  (> num1 num2))

(defn mayor-que-2
  [num1 num2 num3]
  (> num1 num2 num3))

(defn mayor-que-3
  [num1 num2 num3 num4]
  (> num1 num2 num3 num4))

(mayor-que-1 10 13)
(mayor-que-2 98 65 31)
(mayor-que-3 101 90 56 23)

(defn mayor-igual-que-1
  [num1 num2]
  (>= num1 num2))

(defn mayor-igual-que-2
  [num1 num2 num3]
  (>= num1 num2 num3))

(defn mayor-igual-que-3
  [num1 num2 num3 num4]
  (>= num1 num2 num3 num4))

(mayor-igual-que-1 66 66)
(mayor-igual-que-2 90 11 67)
(mayor-igual-que-3 99 96 69 60)

(defn funcion-assoc-1
  [mapa llave valor]
  (assoc mapa llave valor))

(defn funcion-assoc-2
  [vector posicion valor]
  (assoc vector posicion valor))

(defn funcion-assoc-3
  [llave valor]
  (assoc {10 100 20 200 30 300} llave valor))

(funcion-assoc-1 {10 20 30 40} 10 100)
(funcion-assoc-2 [90 70 50 30 10] 2 500)
(funcion-assoc-3 40 400)

(defn funcion-assoc-in-1
  [vector posicion valor]
  (assoc-in vector posicion valor))

(defn funcion-assoc-in-2
  [mapa llave valor]
  (assoc-in mapa llave valor))

(defn funcion-assoc-in-3
  [fila columna valor]
  (assoc-in [[0 0 0]
             [0 0 0]
             [0 0 0]] [fila columna] valor))

(funcion-assoc-in-1 [{:nombre "Fanny" :edad 23} {:nombre "Bere" :edad 20}] [1 :edad] 17)
(funcion-assoc-in-2 {:usuario {:edad 50}} [:usuario :id] 123)
(funcion-assoc-in-3 2 2 1)

(defn funcion-concat-1
  [elemento1 elemento2]
  (concat elemento1 elemento2))

(defn funcion-concat-2
  [lista mapa conjunto]
  (concat lista mapa conjunto [lista]))

(defn funcion-concat-3
  [coleccion cadena]
  (into [] (concat coleccion cadena)))

(funcion-concat-1 "Hola" "¿Cómo estás?")
(funcion-concat-2 '(90 80 70) {10 100 20 200} #{6 7 8})
(funcion-concat-3 #{1 2 3 4 5} "Hola")

(defn funcion-conj-1
  [mapa llave valor]
  (conj mapa [llave valor]))

(defn funcion-conj-2
  [conjunto valor]
  (conj conjunto valor))

(defn funcion-conj-3
  [coleccion cadena numero]
  (conj '() coleccion cadena numero))

(funcion-conj-1 {1 2 3 4} 5 6)
(funcion-conj-2 #{9 8 7 6} [1 2])
(funcion-conj-3 '("a" "b" "c") "Hola" 66)

(defn funcion-cons-1
  [valor lista]
  (cons valor lista))

(defn funcion-cons-2
  [llave valor mapa]
  (cons [llave valor] mapa))

(defn funcion-cons-3
  [coleccion]
  (cons coleccion '({:nombre "Estefania" :id 123})))

(funcion-cons-1 123 '("a" "b" "c"))
(funcion-cons-2 50 120 {9 90 7 70 4 40})
(funcion-cons-3 {:nombre "Berenice" :id 987})

(defn funcion-contains?-1
  [mapa llave]
  (contains? mapa llave))

(defn funcion-contains?-2
  [vector posicion]
  (contains? vector posicion))

(defn funcion-contains?-3
  [cadena posicion]
  (contains? cadena posicion))

(funcion-contains?-1 {:nombre "Estefania" :edad 23 :id 12345} :edad)
(funcion-contains?-2 ["Hola" "Hello" "Ciao"] "Hello")
(funcion-contains?-3 "Hola ¿Cómo estás?" 20)

(defn funcion-count-1
  [cadena]
  (count cadena))

(defn funcion-count-2
  [coleccion]
  (count coleccion))

(defn funcion-count-3
  [vector cantidad]
  (= (count vector) cantidad))

(funcion-count-1 "Hola ¿Cómo te llamas?")
(funcion-count-2 '("Hola" :etiqueta 1234 \A))
(funcion-count-3 [\E \s \t \e \f \a \n \i \a] 9)

(defn funcion-disj-1
  [conjunto valor]
  (disj conjunto valor))

(defn funcion-disj-2
  [conjunto valor1 valor2]
  (disj conjunto valor1 valor2))

(defn funcion-disj-3
  [conjuntoVectores valor]
  (disj conjuntoVectores valor))

(funcion-disj-1 #{"hola" "adiós" "hasta luego" "hasta pronto"} "hola")
(funcion-disj-2 #{:a :b :c :d :e :f :g} :z :d)
(funcion-disj-3 #{[1 2 3] [4 5 6] [7 8] [9] [0]} [7 8])

(defn funcion-dissoc-1
  [mapa llave]
  (dissoc mapa llave))

(defn funcion-dissoc-2
  [llave]
  (dissoc {:a \A :b \B :c \C} llave))

(defn funcion-dissoc-3
  [mapa llave1 llave2]
  (dissoc mapa llave1 llave2))

(funcion-dissoc-1 {:w 80 :x 100 :y 120 :z 140} :z)
(funcion-dissoc-2 :c)
(funcion-dissoc-3 {0 '(1 2 3) 1 '(0 9 8) 2 '(0 0 0) 3 '(5 6) 4 "hola" 5 "adios"} 4 5)

(defn funcion-distinct-1
  [lista]
  (distinct lista))

(defn funcion-distinct-2
  [vector]
  (distinct vector))

(defn funcion-distinct-3
  [cadena]
  (distinct cadena))

(funcion-distinct-1 '(1 2 3 4 5 7 6 4 3 1 7 9 9 6 4 3 6 7))
(funcion-distinct-2 [0 0 0 0 9 8 6 5 6 8 9 0 8 7 6 6 7 9 0])
(funcion-distinct-3 "abcdabcdabcd")

(defn funcion-distinct?-1
  [a b c d e f]
  (distinct? a b c d e f))

(defn funcion-distinct?-2
  [lista1 lista2 lista3]
  (distinct? lista1 lista2 lista3))

(defn funcion-distinct?-3
  [cadena1 cadena2 cadena3 cadena4 cadena5]
  (distinct? cadena1 cadena2 cadena3 cadena4 cadena5))

(funcion-distinct?-1 6 6 9 8 4 1)
(funcion-distinct?-2 '("a" "b" "c") '("g" "h" "i") '("d" "e" "f"))
(funcion-distinct?-3 "hola" "adios" "hello" "Adios" "ADIOS")

(defn funcion-drop-last-1
  [mapa]
  (drop-last mapa))

(defn funcion-drop-last-2
  [lista cantidad-eliminar]
  (drop-last cantidad-eliminar lista))

(defn funcion-drop-last-3
  [vector eliminar]
  (drop-last eliminar vector))

(funcion-drop-last-1 {:a 1 :b 2 :c 3 :d 4 :e 5})
(funcion-drop-last-2 '(#{10 20 30 40} "hola" [1 2 3] #{9 8 7 6} "adios" 987) 4)
(funcion-drop-last-3 [[1 2 3] [4 5 6] [7 8] [9] [0]] 2)

(defn funcion-empty-1
  [vector]
  (map empty vector))

(defn funcion-empty-2
  [lista]
  (empty lista))

(defn funcion-empty-3
  [cadena]
  (empty cadena))

(funcion-empty-1 [#{10 20 30 40} "hola" [1 2 3] #{9 8 7 6} "adios" 987])
(funcion-empty-2 '([1 2 3] [4 5 6] [7 8] [9] [0]))
(funcion-empty-3 "Hola, ¿cómo te llamas?")

(defn funcion-empty?-1
  [vector]
  (every? empty? vector))

(defn funcion-empty?-2
  [lista]
  (map empty? lista))

(defn funcion-empty?-3
  [cadena]
  (empty? cadena))

(funcion-empty?-1 ["" [] () '() {} #{} nil])
(funcion-empty?-2 '("hola" #{} [:a :b :c] {:h "hi" :b "bye"} '()))
(funcion-empty?-3 "")

(defn funcion-even?-1
  [numero]
  (even? numero))

(defn funcion-even?-2
  [rango]
  (filter even? (range rango)))

(defn funcion-even?-3
  [numero]
  (even? numero))

(funcion-even?-1 3)
(funcion-even?-2 50)
(funcion-even?-3 20)

(defn funcion-false?-1
  [booleano]
  (false? booleano))

(defn funcion-false?-2
  [numero]
  (false? numero))

(defn funcion-false?-3
  [vector]
  (map false? vector))

(funcion-false?-1 false)
(funcion-false?-2 10)
(funcion-false?-3 [false true true false])

(defn funcion-find-1
  [mapa llave]
  (find mapa llave))

(defn funcion-find-2
  [vector posicion]
  (find vector posicion))

(defn funcion-find-3
  [mapa llave]
  (find mapa llave))

(funcion-find-1 {:a \A :b \B :c \C :d \D} :e)
(funcion-find-2 [1 2 3 4 5 6] 2)
(funcion-find-3 {1 "ab" 2 "cd" 3 "ef"} 3)

(defn funcion-first-1
  [vector]
  (first vector))

(defn funcion-first-2
  [lista]
  (first lista))

(defn funcion-first-3
  [conjunto]
  (first conjunto))

(funcion-first-1 [:abc [1 2 3] "hola" \X])
(funcion-first-2 '(123 {1 :a} "adios" '("lista")))
(funcion-first-3 #{'("lista") [1 2 3 4 5] {:a "a" :b "b"} #{1 6 9 0}})

(defn funcion-flatten-1
  [listaColecciones]
  (flatten listaColecciones))

(defn funcion-flatten-2
  [vectorColecciones]
  (flatten vectorColecciones))

(defn funcion-flatten-3
  [mapaColecciones]
  (flatten (seq mapaColecciones)))

(funcion-flatten-1 '("hola" "adios" [1 2 3 4 5]))
(funcion-flatten-2 [:abc [1 2 3] "hola" \X [4 5 6]])
(funcion-flatten-3 {'("lista") [1 2 3 4 5] {:a "a" :b "b"} #{1 6 9 0}})

(defn funcion-frequencies-1
  [vector]
  (frequencies vector))

(defn funcion-frequencies-2
  [lista]
  (frequencies lista))

(defn funcion-frequencies-3
  [cadena]
  (frequencies cadena))

(funcion-frequencies-1 [1 1 5 7 7 9 3 4 5 6 7 3])
(funcion-frequencies-2 '(100 110 300 300 200 110 100))
(funcion-frequencies-3 "Estefania")

(defn funcion-get-1
  [mapa llave]
  (get mapa llave))

(defn funcion-get-2
  [vector posicion]
  (get vector posicion))

(defn funcion-get-3
  [cadena posicion]
  (get cadena posicion))

(funcion-get-1 {1 100 2 200 3 300 :a 1 :b 2 :c 3} :a)
(funcion-get-2 [100 200 300 400 500 600] 2)
(funcion-get-3 "Estefania Berenice" 10)

(defn funcion-get-in-1
  [vector posicion1 posicion2]
  (get-in vector [posicion1 posicion2]))

(defn funcion-get-in-2
  [mapa posicion]
  (get-in mapa posicion))

(defn funcion-get-in-3
  [vector posicion1 posicion2]
  (get-in vector [posicion1 posicion2]))

(funcion-get-in-1 [[1 2 3] [4 5 6 7 8] [9 0]] 1 3)
(funcion-get-in-2 {:mascotas [{:nombre "Fairy" :tipo "perro"} {:nombre "Colitas" :tipo "gato"}]} [:mascotas 0 :nombre])
(funcion-get-in-3 [{1 :Estefania} {2 :Berenice 2.1 :Fanny} {3 :Ortega} {4 :Martínez}] 1 2)

(defn funcion-into-1
  [vector]
  (into (sorted-map) vector))

(defn funcion-into-2
  [mapa lista]
  (into lista mapa))

(defn funcion-into-3
  [conjunto vector]
  (into vector conjunto))

(funcion-into-1 [[1 10] [2 20] [3 30] [4 40] [5 50]])
(funcion-into-2 {:a 100 :b 200 :c 300} '())
(funcion-into-3 #{90 80 70 60 50} [])

(defn funcion-key-1
  [mapa]
  (map key mapa))

(defn funcion-key-2
  [mapa]
  (map key mapa))

(defn funcion-key-3
  [mapa]
  (map key mapa))

(funcion-key-1 {11 :a 22 :b 33 :c 44 :d})
(funcion-key-2 {:mascotas [{:nombre "Fairy" :tipo "perro"} {:nombre "Colitas" :tipo "gato"}]
                :dueños [{:nombre "Estefania"} {:nombre "Gabriela"}]})
(funcion-key-3 {#{"uno"} 1 #{"dos"} 2 #{"tres"} 3})

(defn funcion-keys-1
  [mapa]
  (keys mapa))

(defn funcion-keys-2
  [mapa]
  (keys mapa))

(defn funcion-keys-3
  [mapa]
  (keys mapa))

(funcion-keys-1 {11 :a 22 :b 33 :c 44 :d})
(funcion-keys-2 {:mascotas [{:nombre "Fairy" :tipo "perro"} {:nombre "Colitas" :tipo "gato"}]
                :dueños [{:nombre "Estefania"} {:nombre "Gabriela"}]})
(funcion-keys-3 {#{"uno"} 1 #{"dos"} 2 #{"tres"} 3})

(defn funcion-max-1
  [vector]
  (apply max vector))

(defn funcion-max-2
  [numero1 numero2]
  (max numero1 numero2))

(defn funcion-max-3
  [lista]
  (apply max lista))

(funcion-max-1 [90 65 37 80 100 1478 90238])
(funcion-max-2 100 101)
(funcion-max-3 '(78 3435 987674 21452 98172 4724))

(defn funcion-merge-1
  [mapa1 mapa2]
  (merge mapa1 mapa2))

(defn funcion-merge-2
  [mapa1 mapa2 mapa3]
  (merge mapa1 mapa2 mapa3))

(defn funcion-merge-3
  [mapa]
  (merge {:z #{1 2 3 4 5}} mapa))

(funcion-merge-1 {:a 100 :b 200 :c 300} {:a 100 :d 400})
(funcion-merge-2 {:h "hola" :a "adios" :b "bye"} {:x [10 11 12] :y [13 14 15]} {:z #{1 2 3 4 5}})
(funcion-merge-3 {:x #{90 80 70 60} :y #{5 10 15 20 25}})

(defn funcion-min-1
  [vector]
  (apply min vector))

(defn funcion-min-2
  [numero1 numero2]
  (min numero1 numero2))

(defn funcion-min-3
  [lista]
  (apply min lista))

(funcion-min-1 [90 65 37 80 100 1478 90238])
(funcion-min-2 100 101)
(funcion-min-3 '(78 3435 987674 21452 98172 4724))

(defn funcion-neg?-1
  [numero]
  (neg? numero))

(defn funcion-neg?-2
  [vector]
  (map neg? vector))

(defn funcion-neg?-3
  [lista]
  (map neg? lista))

(funcion-neg?-1 100)
(funcion-neg?-2 [90 65 37 -80 -100 -1478 -90238])
(funcion-neg?-3 '(-1/2 -9/4 -2/2 -8/2))

(defn funcion-nil?-1
  [cadena]
  (nil? cadena))

(defn funcion-nil?-2
  [vector]
  (nil? vector))

(defn funcion-nil?-3
  [x]
  (nil? x))

(funcion-nil?-1 "Estefania")
(funcion-nil?-2 [])
(funcion-nil?-3 nil)

(defn funcion-not-empty-1
  [cadena]
  (not-empty cadena))

(defn funcion-not-empty-2
  [lista]
  (not-empty lista))

(defn funcion-not-empty-3
  [vector]
  (map not-empty vector))

(funcion-not-empty-1 "")
(funcion-not-empty-2 '(4 5 7 8 6 5 3))
(funcion-not-empty-3 ['() #{} {} []])

(defn funcion-nth-1
  [vector posicion]
  (nth vector posicion))

(defn funcion-nth-2
  [lista posicion]
  (nth lista posicion))

(defn funcion-nth-3
  [cadena posicion]
  (nth cadena posicion))

(funcion-nth-1 ['() #{} {} []] 1)
(funcion-nth-2 '(:a 90 :b 100 :c 200 :d 310) 2)
(funcion-nth-3 "ESTEFANIA ORTEGA" 4)

(defn funcion-odd?-1
  [numero]
  (odd? numero))

(defn funcion-odd?-2
  [vector]
  (map odd? vector))

(defn funcion-odd?-3
  [rango]
  (filter odd? (range rango)))

(funcion-odd?-1 5)
(funcion-odd?-2 [7 4 7 9 6 2 1])
(funcion-odd?-3 30)

(defn funcion-partition-1
  [vector partes]
  (partition partes vector))

(defn funcion-partition-2
  [lista partes]
  (partition partes lista))

(defn funcion-partition-3
  [cadena partes]
  (partition partes cadena))

(funcion-partition-1 [1 2 3 4 5 6 7 8 9] 3)
(funcion-partition-2 '(:a :b :c :d :e :f :g :h :i :j :k :l) 5)
(funcion-partition-3 "abcdefghijklmnñopqrstuvwxyz" 2)

(defn funcion-partition-all-1
  [vector partes]
  (partition-all partes vector))

(defn funcion-partition-all-2
  [lista partes]
  (partition-all partes lista))

(defn funcion-partition-all-3
  [rango partes]
  (partition-all partes (range rango)))

(funcion-partition-all-1 [1 2 3 4 5 6 7 8 9] 4)
(funcion-partition-all-2 '(:a :b :c :d :e :f :g :h :i :j :k :l :m) 3)
(funcion-partition-all-3 15 2)

(defn funcion-peek-1
  [vector]
  (peek vector))

(defn funcion-peek-2
  [lista]
  (peek lista))

(defn funcion-peek-3
  [vacio]
  (map peek vacio))

(funcion-peek-1 [1 2 3 4 5 6 7 8 9 5 8 9 6 4 2 7 9 8 5 8 5 32 5 7])
(funcion-peek-2 '(:a :b :c :d :e :f :g :h :i :j :k :l :m))
(funcion-peek-3 ['() [] [] '()])

(defn funcion-pop-1
  [vector]
  (pop vector))

(defn funcion-pop-2
  [lista]
  (pop lista))

(defn funcion-pop-3
  [vacio]
  (map pop vacio))

(funcion-pop-1 [1 2 3 4 5 6 7 8 9 5 8 9 6 4 2 7 9 8 5 8 5 32 5 7])
(funcion-pop-2 '(:a :b :c :d :e :f :g :h :i :j :k :l :m))
(funcion-pop-3 ['(1 2 3) [4 5 6] [7 8 9] '(0)])

(defn funcion-pos?-1
  [numero]
  (pos? numero))

(defn funcion-pos?-2
  [numero]
  (pos? numero))

(defn funcion-pos?-3
  [vector]
  (map pos? vector))

(funcion-pos?-1 10)
(funcion-pos?-2 -1/2)
(funcion-pos?-3 [1/2 -6/4 -4/3 -9.0 9])

(defn funcion-quot-1
  [numero divisor]
  (quot numero divisor))

(defn funcion-quot-2
  [numero divisor]
  (quot numero divisor))

(defn funcion-quot-3
  [numero divisor]
  (quot numero divisor))

(funcion-quot-1 9 3)
(funcion-quot-2 -5.9 2)
(funcion-quot-3 18/3 3)

(defn funcion-range-1
  [inicio fin]
  (range inicio fin))

(defn funcion-range-2
  [final]
  (range final))

(defn funcion-range-3
  [inicio final saltos]
  (range inicio final saltos))

(funcion-range-1 10 30)
(funcion-range-2 16)
(funcion-range-3 70 100 5)

(defn funcion-rem-1
  [numero divisor]
  (rem numero divisor))

(defn funcion-rem-2
  [numero divisor]
  (rem numero divisor))

(defn funcion-rem-3
  [numero divisor]
  (rem numero divisor))

(funcion-rem-1 9 2)
(funcion-rem-2 -5.9 2)
(funcion-rem-3 18/3 5)

(defn funcion-repeat-1
  [valor]
  (repeat valor))

(defn funcion-repeat-2
  [valor repeticiones]
  (take repeticiones (repeat valor)))

(defn funcion-repeat-3
  [coleccion repeticiones]
  (take repeticiones (repeat coleccion)))

(funcion-repeat-1 "hi")
(funcion-repeat-2 "bye" 10)
(funcion-repeat-3 [1 2 3] 6)

(defn funcion-replace-1
  [cadena subcadena sustituto]
  (clojure.string/replace cadena subcadena sustituto))

(defn funcion-replace-2
  [cadena subcadena sustituto]
  (clojure.string/replace cadena subcadena sustituto))

(defn funcion-replace-3
  [cadena subcadena sustituto]
  (clojure.string/replace cadena subcadena sustituto))

(funcion-replace-1 "Mi perro se llama Fairy" #"perro" "gato")
(funcion-replace-2 "Me llamo Estefania." #"[aeiou]"  #(str %1 %1))
(funcion-replace-3 "estefania berenice ortega martínez" #"\b." #(.toUpperCase %1))

(defn funcion-rest-1
  [vector]
  (rest vector))

(defn funcion-rest-2
  [lista]
  (rest lista))

(defn funcion-rest-3
  [conjunto]
  (rest conjunto))

(funcion-rest-1 [[7 6 4 3] {5 50 7 70 9 90} '(7 5 3 3)])
(funcion-rest-2 '(8 6 4 3 5 7 9 0 9))
(funcion-rest-3 #{:a :b :c :d :e :f :g})

(defn funcion-select-keys-1
  [mapa llaves]
  (select-keys mapa llaves))

(defn funcion-select-keys-2
  [vector llaves]
  (select-keys vector llaves))

(defn funcion-select-keys-3
  [lista llaves]
  (select-keys lista llaves))

(funcion-select-keys-1 {:h "hola", :a "adios", :b "bye", :x [10 11 12], :y [13 14 15], :z #{1 4 3 2 5}} [:h :b :y])
(funcion-select-keys-2 [[7 6 4 3] {5 50 7 70 9 90} '(7 5 3 3) {:a 10 :b 20 :c 30}] [3 0])
(funcion-select-keys-3 {:a '(8 6 4 3) :b #{5 7 0 9} :c [9 6 0]} [:b :a :c])

(defn funcion-shuffle-1
  [lista]
  (shuffle lista))

(defn funcion-shuffle-2
  [conjunto repeticiones]
  (repeatedly repeticiones (partial shuffle conjunto)))

(defn funcion-shuffle-3
  [vector]
  (shuffle vector))

(funcion-shuffle-1 '(8 6 4 3 5 7 9 0 9))
(funcion-shuffle-2 #{90 85 80} 6)
(funcion-shuffle-3 [7 6 4 3 5 50 7 70 9 90 7 5 3 3 :a 10 :b 20 :c 30])

(defn funcion-sort-1
  [lista]
  (sort lista))

(defn funcion-sort-2
  [lista]
  (sort lista))

(defn funcion-sort-3
  [vector]
  (sort vector))

(funcion-sort-1 '(8 6 4 3 5 7 9 0 9))
(funcion-sort-2 '([8 6 3 2] [5 43 7 9] [ 1 3 4 6 7]))
(funcion-sort-3 [7 6 4 3 5 50 7 70 9 90 7 5 3 3 10 20 30])

(defn funcion-split-at-1
  [lista x]
  (split-at x lista))

(defn funcion-split-at-2
  [lista x]
  (split-at x lista))

(defn funcion-split-at-3
  [vector x]
  (split-at x vector))

(funcion-split-at-1 '(8 6 4 3 5 7 9 0 9) 3)
(funcion-split-at-2 '([8 6 3 2] [5 43 7 9] [1 3 4 6 7]) 2)
(funcion-split-at-3 [7 6 4 3 5 50 7 70 9 90 7 5 3 3 10 20 30] 6)

(defn funcion-str-1
  [numero1 numero2 numero3 numero4]
  (str numero1 numero2 numero3 numero4))

(defn funcion-str-2
  [cadena1 cadena2]
  (str cadena1 cadena2))

(defn funcion-str-3
  [vector ]
  (apply str vector))

(funcion-str-1 987 432 678 0000)
(funcion-str-2 "Hola ¿cómo te llamas?" "Hasta luego")
(funcion-str-3 [7 6 4 :a 5 50 :b 70  90 :c 5 \W 3 10 :d \Z])

(defn funcion-subs-1
  [cadena inicio]
  (subs cadena inicio))

(defn funcion-subs-2
  [cadena inicio final]
  (subs cadena inicio final))

(defn funcion-subs-3
  [cadena inicio final]
  (subs cadena inicio final))

(funcion-subs-1 "Mi perrita se llama Fayri" 6)
(funcion-subs-2 "Mi color favorito es el morado" 18 20)
(funcion-subs-3 "9 7 4 2 1 4 6 8 -1/4 1/9 7/3 -9/2" 15 21)

(defn funcion-subvec-1
  [vector inicio]
  (subvec vector inicio))

(defn funcion-subvec-2
  [vector inicio final]
  (subvec vector inicio final))

(defn funcion-subvec-3
  [vector inicio]
  (subvec vector inicio))

(funcion-subvec-1 [9 7 6 3 1 34 5 7 9 1.7788 8.421 -1/30] 9)
(funcion-subvec-2 [9 7 4 2 1 4 6 8 -1/4 1/9 7/3 -9/2 754.345 9/20 -75] 11 13)
(funcion-subvec-3 [[7 6 4 3] {5 50 7 70 9 90} '(7 5 3 3) {:a 10 :b 20 :c 30}] 2)

(defn funcion-take-1
  [vector cantidad]
  (take cantidad vector))

(defn funcion-take-2
  [lista cantidad]
  (take cantidad lista))

(defn funcion-take-3
  [cadena cantidad]
  (take cantidad cadena))

(funcion-take-1 [9 7 4 2 1 4 6 8 -1/4 1/9 7/3 -9/2 754.345 9/20 -75] 8)
(funcion-take-2 '(8 6 4 3 5 7 9 0 9) 3)
(funcion-take-3 "Mi color favorito no es el amarillo" 7)

(defn funcion-true?-1
  [numero1 numero2]
  (true? (= numero1 numero2)))

(defn funcion-true?-2
  [booleano]
  (true? booleano))

(defn funcion-true?-3
  [cadena1 cadena2]
  (true? (= cadena1 cadena2)))

(funcion-true?-1 98 76)
(funcion-true?-2 true)
(funcion-true?-3 "Hola" "hola")

(defn funcion-val-1
  [mapa]
  (map val mapa))

(defn funcion-val-2
  [mapa]
  (val (first mapa)))

(defn funcion-val-3
  [mapa]
  (val (last mapa)))

(funcion-val-1 {:h "hola" :a "adios" :b "bye" :x [10 11 12] :y [13 14 15] :z #{1 4 3 2 5}})
(funcion-val-2 {:uno {:a 100 :b 200 :c 300} :dos {:a 100 :d 400}})
(funcion-val-3 {\A :a \B :b \C :c \D :d \E :e})

(defn funcion-vals-1
  [mapa]
  (vals mapa))

(defn funcion-vals-2
  [mapa]
  (vals mapa))

(defn funcion-vals-3
  [mapa]
  (vals mapa))

(funcion-vals-1 {:h "hola" :a "adios" :b "bye" :x [10 11 12] :y [13 14 15] :z #{1 4 3 2 5}})
(funcion-vals-2 {:uno {:a 100 :b 200 :c 300} :dos {:a 100 :d 400}})
(funcion-vals-3 {\A :a \B :b \C :c \D :d \E :e})

(defn funcion-zero?-1
  [numero]
  (zero? numero))

(defn funcion-zero?-2
  [vector]
  (map zero? vector))

(defn funcion-zero?-3
  [lista]
  (map zero? lista))

(funcion-zero?-1 0.001)
(funcion-zero?-2 [0 9 8 1 0.0 0.1])
(funcion-zero?-3 '(7 0.000 0 1.0))

(defn funcion-zipmap-1
  [llaves valores]
  (zipmap llaves valores))

(defn funcion-zipmap-2
  [llaves valores]
  (zipmap llaves valores))

(defn funcion-zipmap-3
  [llaves valores]
  (zipmap llaves valores))

(funcion-zipmap-1 [1 2 3 4 5] [111 222 333 444 555])
(funcion-zipmap-2 '("a" "b" "c") '(1 2 3))
(funcion-zipmap-3 #{\A \B \C \D \E} #{:a :b :c :d :e})